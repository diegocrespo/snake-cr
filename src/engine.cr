require "raylib-cr"
# Where all my Game Agnostic Functions will live
alias RL = Raylib
alias Key = RL::KeyboardKey

class Text
  @font : RL::Font
  @text : String
  @position : RL::Vector2
  @origin : RL::Vector2
  @rotation : Float64
  @font_size : Float64
  @spacing : Float64 # spacing between the words
  @color : RL::Color = RL::BLACK
  getter size : RL::Vector2

  def initialize(@font = RL.get_font_default, @text = "debug text", @position = RL::Vector2.new(x: 0, y: 0), @origin = RL::Vector2.new(x: 0, y: 0), @rotation = 0.0, @font_size = 12.0, @spacing = 2.0, @color = RL::BLACK)
    @size = RL.measure_text_ex(@font, @text, @font_size, @spacing)
  end

  def draw(pad_hor = 0, pad_vert = 0)
    if pad_hor || pad_vert
      @position = RL::Vector2.new(x: @position.x + pad_hor, y: @position.y + pad_vert)
    end

    RL.draw_text_pro(@font, @text, @position, @origin, @rotation, @font_size, @spacing, @color)
  end
end
