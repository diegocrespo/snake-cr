require "raylib-cr"
require "./engine"
require "random"

# TODO: Write documentation for `Snake_Game`
module Snake_Game
  VERSION = "0.5.0"

  alias RL = Raylib
  alias Key = RL::KeyboardKey
  Screen_Width  = 1280
  Screen_Height =  800
  Grid_Size     =   40
  Rows          = (Screen_Width / Grid_Size).to_i32
  Columns       = (Screen_Height / Grid_Size).to_i32
  DEBUG         = false
  LINE_THICKNESS = 10
  points = 0
  enum State
    TITLE
    PLAYING
    DEAD
  end
  game_state = State::TITLE

  RL.init_window(Screen_Width, Screen_Height, "Snake")
  RL.set_target_fps(60)

  def self.debug(snake, debug = false)
    if debug
      RL.draw_text("Snake X is #{snake.pos.x}", 0, 0, 50, RL::RED)
      RL.draw_text("Snake y is #{snake.pos.y}", 0, 60, 50, RL::RED)
    end
  end

  def self.draw_grid(step = Grid_Size)
    # Loop through the x coordinates from 0 to screen width in predefined steps
    (0..Screen_Width).step(step) do |x|
      # Loop through the y coordinates from 0 to screen height in predefined steps
      (0..Screen_Height).step(step) do |y|
        # Draw a line from (x, y) to (x + 10, y + 10) with a black color
        Raylib.draw_line_v(Raylib::Vector2.new(x: 1*x, y: 0), Raylib::Vector2.new(x: 1*x, y: Screen_Height), Raylib::BLACK)
        Raylib.draw_line_v(Raylib::Vector2.new(x: 0, y: 1 * y), Raylib::Vector2.new(x: Screen_Width, y: 1*y), Raylib::BLACK)
      end
    end
  end


  class Food
    property pos = RL::Vector2.new(x: 50, y: 50)
    property size = RL::Vector2.new(x: Grid_Size, y: Grid_Size)
    @color : RL::Color = RL::RED
    getter collision : RL::Rectangle

    def spawn_food
      x = (Random.rand(Rows) * Grid_Size).to_i32
      y = (Random.rand(Columns) * Grid_Size).to_i32
      @pos = RL::Vector2.new(x: x, y: y)
    end

    def initialize
      spawn_food
      @collision = RL::Rectangle.new(x: @pos.x, y: @pos.y, width: @size.x, height: @size.y)
    end

    def consume
      spawn_food
    end

    def update
      @collision = RL::Rectangle.new(x: @pos.x, y: @pos.y, width: @size.x, height: @size.y)
    end

    def draw
      update
      RL.draw_rectangle_v(@pos, @size, @color)
    end
  end

  class Snake
    property pos = RL::Vector2.new(x: 50, y: 50)
    property body_arr = Array(RL::Vector2).new
    getter snake_size = 1
    property size = RL::Vector2.new(x: Grid_Size, y: Grid_Size)
    @color : RL::Color = RL::BLACK
    property speed = Grid_Size
    @collision : RL::Rectangle
    enum Direction
      UP
      RIGHT
      LEFT
      DOWN
    end
    setter dir = Direction::RIGHT

    def handle_keys
      if RL.key_down?(Key::Right)
        @dir = Direction::RIGHT
      elsif RL.key_down?(Key::Left)
        @dir = Direction::LEFT
      elsif RL.key_down?(Key::Up)
        @dir = Direction::UP
      elsif RL.key_down?(Key::Down)
        @dir = Direction::DOWN
      end
    end

    def move
      @body_arr << RL::Vector2.new(x: @pos.x, y: @pos.y)
      if @body_arr.size > @snake_size
        @body_arr = @body_arr[1..@body_arr.size - 1]
      end
      case @dir
      when Direction::RIGHT
        @pos.x += @speed
      when Direction::LEFT
        @pos.x -= @speed
      when Direction::UP
        @pos.y -= @speed
      when Direction::DOWN
        @pos.y += @speed
      end
    end

    def check_collision
      if @pos.x > Screen_Width
        return true
      elsif @pos.x < 0
        return true
      elsif @pos.y < 0
        return true
      elsif @pos.y + @size.y > Screen_Height
        return true
      else
        return false
      end
    end

    def eat(food)
      if RL.check_collision_recs?(@collision, food.collision)
        food.spawn_food
        @snake_size += 1
        true
      end
    end

    def draw
      update
      # draw head
      RL.draw_rectangle_v(@pos, @size, @color)
      if @snake_size > 1
        body_arr.each do |seg|
          RL.draw_rectangle_v(seg, @size, @color)
        end
      end
    end

    def update
      @collision = RL::Rectangle.new(x: @pos.x, y: @pos.y, width: @size.x, height: @size.y)
      if @snake_size > 1
        body_arr.each do |seg|
          body_col = RL::Rectangle.new(x: seg.x, y: seg.y, width: @size.x, height: @size.y)
          if RL.check_collision_recs?(@collision, body_col)
            game_state = State::DEAD
          end
        end
      end
    end

    def initialize
      x = ((Rows / 2) * Grid_Size).to_i32
      y = ((Columns / 2) * Grid_Size).to_i32
      @pos = RL::Vector2.new(x: x, y: y)
      # indirect initialization not supported
      @collision = RL::Rectangle.new(x: @pos.x, y: @pos.y, width: @size.x, height: @size.y)
    end
  end

  snake = Snake.new
  food = Food.new
  start_time = Time.monotonic
  until RL.close_window?
    RL.begin_drawing
    RL.clear_background(RL::RAYWHITE)
    case game_state
    when State::TITLE
      start_text = Text.new(text: "PRESS SPACE TO START", position: RL::Vector2.new(x: Screen_Width/2, y: Screen_Height/2), font_size: 80)
      start_text.draw(pad_hor: -start_text.size.x/2, pad_vert: -start_text.size.y/2)
      if RL.key_down?(Key::Space)
        game_state = State::PLAYING
      end
    when State::PLAYING
      snake.draw
      if Time.monotonic - start_time > 0.05.seconds
        snake.handle_keys
        snake.move
        start_time = Time.monotonic
        if snake.check_collision
          game_state = State::DEAD
        end
      end
      draw_grid()
      food.draw
      if snake.eat(food)
        points += 1
      end
      debug(snake, DEBUG)
      points_text = Text.new(text: "Points: #{points}", position: RL::Vector2.new(x: Screen_Width, y: 0), font_size: 20)
      points_text.draw(pad_hor: (points_text.size.x + 5) * -1)
    when State::DEAD
      start_text = Text.new(text: "PRESS SPACE TO RESTART", position: RL::Vector2.new(x: Screen_Width/2, y: Screen_Height/2), font_size: 80)
      start_text.draw(pad_hor: -start_text.size.x/2, pad_vert: -start_text.size.y/2)
      if RL.key_down?(Key::Space)
        snake = Snake.new
        food = Food.new
        start_time = Time.monotonic
        points = 0
        game_state = State::PLAYING
      end
    end
    RL.end_drawing
  end
  RL.close_window
end
